// End note builder
// Notes in html can be referenced with names or number as the system numbers them
// constrains:
// - Section needs a .chapter class
// - <a class="ref-fn" id="ref-myname" href="#fn-myname">myname</a>
// - or <a class="ref-fn" id="ref-10" href="#fn-10">10</a>
// - note: <div class="note" id="fn-10"><p>My note</p></div>.
// Julie Blanc + Nicolas Taffin

/* HANDLER ----------------------------------------------------------------- */
 class handlers extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  beforeParsed(content){     
      endnotes(content);
  }

}
Paged.registerHandlers(handlers);

/* CHECK ELEMENT FUNCTION */

//Returns true if it is a DOM element    
function isElement(o){
  return (
    typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
    o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName==="string"
);
}



/* ENDNOTE FUNCTION ------------------------------------------------------- */
function endnotes(content){

  let chapters = content.querySelectorAll(".chapter");

  for (let n = 0; n < chapters.length; n++) {
    let refs = chapters[n].querySelectorAll(".ref-fn");

    /* for each call note */
    if (typeof(refs) != 'undefined' && refs != null){
      for (let i = 0; i < refs.length; i++) {

        let numNote = i + 1;
        let idNote = refs[i].id.replace('ref-', '');

        /* add num call reference */
        refs[i].innerHTML = /* '&nbsp;' + */ numNote;
        refs[i].href = '#fn-' + numNote;
        refs[i].id = 'ref-' + numNote;

        /* Get endnote */
        let endNote = chapters[n].querySelector('#fn-' + idNote);
        let contentNote = endNote.innerHTML;

        /* Add num endnote*/
        endNote.id = 'fn-' + numNote;
        let firstChildNote = endNote.childNodes[0];
        let spanNum = document.createElement("span");
        spanNum.className = 'footnote-number'; 
        spanNum.innerHTML = numNote //+ ". ";
        if (!firstChildNote.childNodes[0]) {
          console.log("There is a problem with note " + idNote + " or the note before has a non closed element.");
        } else {
          firstChildNote.insertBefore(spanNum, firstChildNote.childNodes[0]);
        }

        /* Add note return */
        let lastChildNote = endNote.lastChild;
        let linkReturn = document.createElement("a");
        linkReturn.classList.add("return-note");
        linkReturn.href = '#ref-' + numNote;
        linkReturn.innerHTML = '&nbsp;↩';
        
        if (isElement(lastChildNote)) {
          lastChildNote.appendChild(linkReturn);
        } else {
          console.log("There is something wrong with note " + idNote);
          lastChildNote.appendChild(linkReturn);
        }  
      }
    }
  }
  
}